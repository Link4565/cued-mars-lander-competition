# CUED Mars Lander Competition

This branch contains the Unreal Version of the project, written using C++ and Unreal Engine 4.25.

Pre-comiled versions for Windows and Linux can be downloaded from the 'Compiled Versions' folder.

To run on Windows, first run the Pre-requisite installer found under "WindowsNoEditor\Engine\Extras\Redist\en-us", then to run the project, run "TestLander2.exe" found under "WindowsNoEditor\"

To run on Linux, the computer must be running a recent dedicated Nvidia GPU, and up-to-date graphics drivers. To run the project, run "TestLander2.sh" found under "LinuxNoEditor\"