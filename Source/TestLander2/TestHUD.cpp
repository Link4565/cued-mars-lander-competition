// Fill out your copyright notice in the Description page of Project Settings.


#include "TestHUD.h"

//Define functions to make global variables from the simulation visible to the HUD Blueprint

float UTestHUD::GetAltitude() {
	return altitude;
}


float UTestHUD::GetClimbRate() {
	return climb_speed;
}


float UTestHUD::GetGroundSpeed() {
	return surface_ground_speed;
}


float UTestHUD::GetSimulationSpeed() {
	return simulation_speed;
}


float UTestHUD::GetTime() {
	return simulation_time;
}


FVector UTestHUD::GetPosition() {
	return position.toFVector();
}


FVector UTestHUD::GetVelocity() {
	return velocity.toFVector();
}


float UTestHUD::GetThrottle() {
	return throttle;
}


float UTestHUD::GetThrust() {
	return FVector(thrust_wrt_world().toFVector()).Size();
}


float UTestHUD::GetFuel() {
	return fuel;
}


FString UTestHUD::GetScenario() {
	return FString::Printf(TEXT("Scenario %f: %s"), static_cast<float>(scenario), scenario_description[scenario].c_str());
}


bool UTestHUD::GetAutopilotEnabled() {
	return autopilot_enabled;
}


bool UTestHUD::GetAutopilotActive() {
	return autopilot_active;
}


int UTestHUD::GetAutopilotMode() {
	return autopilot_mode;
}


bool UTestHUD::GetAttitudeStabilizerEnabled() {
	return stabilized_attitude;
}

int UTestHUD::GetAttitudeStabilizerMode() {
	return attitude_stabilizer_mode;
}

float UTestHUD::GetAttitudeStabilizerAngle() {
	return stabilized_attitude_angle_lat;
}


int UTestHUD::GetParachuteStatus() {
	return parachute_status;
}