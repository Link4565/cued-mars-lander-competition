// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define _USE_MATH_DEFINES
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <ctime>
#include "FastNoiseWrapper.h"


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestActor.generated.h"

// Graphics constants
#define GAP 5
#define SMALL_NUM 0.0000001
#define N_RAND 20000
#define PREFERRED_WIDTH 1024
#define PREFERRED_HEIGHT 768
#define MIN_INSTRUMENT_WIDTH 1024
#define INSTRUMENT_HEIGHT 300
#define GROUND_LINE_SPACING 20.0
#define CLOSEUP_VIEW_ANGLE 30.0
#define TRANSITION_ALTITUDE 10000.0
#define TRANSITION_ALTITUDE_NO_TEXTURE 4000.0
#define TERRAIN_TEXTURE_SIZE 1024
#define INNER_DIAL_RADIUS 65.0
#define OUTER_DIAL_RADIUS 75.0
#define MAX_DELAY 160000
#define N_TRACK 1000
#define TRACK_DISTANCE_DELTA 100000.0
#define TRACK_ANGLE_DELTA 0.999
#define HEAT_FLUX_GLOW_THRESHOLD 1000000.0

// Mars constants
#define MARS_RADIUS 3386000.0 // (m)
//const double MARS_RADIUS = 3386000.0; // (m)
#define MARS_MASS 6.42E23 // (kg)
#define GRAVITY 6.673E-11 // (m^3/kg/s^2)
#define MARS_DAY 88642.65 // (s)
#define EXOSPHERE 200000.0 // (m)

// Lander constants
#define LANDER_SIZE 1.0 // (m)
#define UNLOADED_LANDER_MASS 100.0 // (kg)
#define FUEL_CAPACITY 100.0 // (l)
#define FUEL_RATE_AT_MAX_THRUST 0 // (l/s)
//#define FUEL_RATE_AT_MAX_THRUST 0.5 // (l/s)
#define FUEL_DENSITY 1.0 // (kg/l)
// MAX_THRUST, as defined below, is 1.5 * weight of fully loaded lander at surface
#define MAX_THRUST (1.5 * (FUEL_DENSITY*FUEL_CAPACITY+UNLOADED_LANDER_MASS) * (GRAVITY*MARS_MASS/(MARS_RADIUS*MARS_RADIUS))) // (N)
#define ENGINE_LAG 0.1 // (s)
#define ENGINE_DELAY 0.1 // (s)
#define DRAG_COEF_CHUTE 2.0
#define DRAG_COEF_LANDER 1.0
#define MAX_PARACHUTE_DRAG 20000.0 // (N)
#define MAX_PARACHUTE_SPEED 500.0 // (m/s)
#define THROTTLE_GRANULARITY 20 // for manual control
#define MAX_IMPACT_GROUND_SPEED 1.0 // (m/s)
#define MAX_IMPACT_DESCENT_RATE 1.0 // (m/s)

#define _USE_MATH_DEFINES

using namespace std;

class vector3d {
	// Utility class for three-dimensional vector operations
public:
	vector3d() { x = 0.0; y = 0.0; z = 0.0; }
	vector3d(double a, double b, double c = 0.0) { x = a; y = b; z = c; }
	vector3d(FVector a) { x = a.X; y = a.Y; z = a.Z; }
	bool operator== (const vector3d& v) const { if ((x == v.x) && (y == v.y) && (z == v.z)) return true; else return false; }
	bool operator!= (const vector3d& v) const { if ((x != v.x) || (y != v.y) || (z != v.z)) return true; else return false; }
	vector3d operator+ (const vector3d& v) const { return vector3d(x + v.x, y + v.y, z + v.z); }
	vector3d operator- (const vector3d& v) const { return vector3d(x - v.x, y - v.y, z - v.z); }
	friend vector3d operator- (const vector3d& v) { return vector3d(-v.x, -v.y, -v.z); }
	vector3d& operator+= (const vector3d& v) { x += v.x; y += v.y; z += v.z; return *this; }
	vector3d& operator-= (const vector3d& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }
	vector3d operator^ (const vector3d& v) const { return vector3d(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); }
	double operator* (const vector3d& v) const { return (x * v.x + y * v.y + z * v.z); }
	friend vector3d operator* (const vector3d& v, const double& a) { return vector3d(v.x * a, v.y * a, v.z * a); }
	friend vector3d operator* (const double& a, const vector3d& v) { return vector3d(v.x * a, v.y * a, v.z * a); }
	vector3d& operator*= (const double& a) { x *= a; y *= a; z *= a; return *this; }
	vector3d operator/ (const double& a) const { return vector3d(x / a, y / a, z / a); }
	vector3d& operator/= (const double& a) { x /= a; y /= a; z /= a; return *this; }
	double abs2() const { return (x * x + y * y + z * z); }
	double abs() const { return sqrt(this->abs2()); }
	vector3d norm() const { double s(this->abs()); if (s == 0) return *this; else return vector3d(x / s, y / s, z / s); }
	friend ostream& operator << (ostream& out, const vector3d& v) { out << v.x << ' ' << v.y << ' ' << v.z; return out; }
	double x, y, z;
	FVector toFVector() const { return FVector(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z)); }
private:
};


// Data type for recording lander's previous positions
struct track_t {
	unsigned short n;
	unsigned short p;
	vector3d pos[N_TRACK];
};

// Quaternions for orbital view transformation
struct quat_t {
	vector3d v;
	double s;
};

// Data structure for the state of the close-up view's coordinate system
struct closeup_coords_t {
	bool initialized;
	bool backwards;
	vector3d right;
};

enum parachute_status_t { NOT_DEPLOYED = 0, DEPLOYED = 1, LOST = 2 };

enum autopilot_mode_t { LAND = 0, TAKE_OFF = 1 };

enum attitude_stabilizer_mode_t { ORBIT = 1, CLOSE_UP = 0, LAT_LONG = 2 };


#ifdef DECLARE_GLOBAL_VARIABLES // actual declarations of all global variables for lander_graphics.cpp

// GL windows and objects
int main_window, closeup_window, orbital_window, instrument_window, view_width, view_height, win_width, win_height;
short throttle_control;
track_t track;
bool texture_available;

// Simulation parameters
bool help = false;
bool paused = false;
bool landed = false;
bool crashed = false;
bool exploded = false;
int last_click_x = -1;
int last_click_y = -1;
short simulation_speed = 5;
short saved_simulation_speed;
double delta_t, simulation_time;
unsigned short scenario = 0;
string scenario_description[10];
bool static_lighting = false;
closeup_coords_t closeup_coords;
float randtab[N_RAND];
bool do_texture = true;
unsigned long throttle_buffer_length, throttle_buffer_pointer;
double* throttle_buffer = NULL;
unsigned long long time_program_started;
int frames_until_update;

// Lander state - the visualization routines use velocity_from_positions, so not sensitive to 
// any errors in the velocity update in numerical_dynamics
vector3d position, previous_position, orientation, velocity, velocity_from_positions, last_position;
double climb_speed, static_ground_speed, surface_ground_speed, altitude, throttle, fuel;
bool stabilized_attitude, autopilot_enabled, parachute_lost;
parachute_status_t parachute_status;
double stabilized_attitude_angle_lat = 0, stabilized_attitude_angle_long = 0;
vector<double> alt_list, target_v_list, v_list;
autopilot_mode_t autopilot_mode;
attitude_stabilizer_mode_t attitude_stabilizer_mode;
double target_altitude = (MARS_RADIUS * 0.2);
bool autopilot_active = false;
vector3d drag_force;
double surface_ground_speed_lat, surface_ground_speed_long;
UFastNoiseWrapper* wind_gusts_n = nullptr;
UFastNoiseWrapper* wind_gusts_e = nullptr;
double current_radius;
float mars_rotation = 0.0f;
AActor* mars_pointer;
APawn* player;
vector3d surface_approach_location;
float surface_approach_rotation;
vector3d lander_up_vector;

// Orbital and closeup view parameters
double orbital_zoom, save_orbital_zoom, closeup_offset, closeup_xr, closeup_yr, terrain_angle;
quat_t orbital_quat;

float camera_rot_x = 0, camera_rot_y = 0;
FVector camera_direction;
float camera_zoom = 1000;
vector3d previous_camera_rotation;

TSubclassOf<class AActor> Explosion_BP;

#else // extern declarations of those global variables used in lander.cpp

extern bool stabilized_attitude, autopilot_enabled;
extern double delta_t, simulation_time, throttle, fuel;
extern unsigned short scenario;
extern string scenario_description[];
extern vector3d position, previous_position, orientation, velocity;
extern parachute_status_t parachute_status;
extern double stabilized_attitude_angle_lat, stabilized_attitude_angle_long;
extern vector<double> alt_list, target_v_list, v_list;
extern autopilot_mode_t autopilot_mode;
extern attitude_stabilizer_mode_t attitude_stabilizer_mode;
extern double static_ground_speed, surface_ground_speed, climb_speed;
extern double target_altitude;
extern bool autopilot_active;
extern vector3d drag_force;
extern double surface_ground_speed_lat, surface_ground_speed_long;
extern UFastNoiseWrapper* wind_gusts_n;
extern UFastNoiseWrapper* wind_gusts_e;
extern double current_radius;
extern float camera_rot_x, camera_rot_y;
extern FVector camera_direction;
extern float mars_rotation;
extern bool landed, crashed;
extern short throttle_control;
extern bool paused;
extern short simulation_speed;
extern bool help;
extern bool static_lighting;
extern bool do_texture;
extern autopilot_mode_t autopilot_mode;
extern int frames_until_update;
extern float camera_zoom;
extern double altitude;
extern short saved_simulation_speed;



#endif


// Function prototypes
void xyz_euler_to_matrix(vector3d ang, double m[]);
vector3d matrix_to_xyz_euler(double m[]);
void microsecond_time(unsigned long long& t);
double atmospheric_density(vector3d pos);
void update_closeup_coords(void);
bool safe_to_deploy_parachute(void);
void update_visualization(void);
void attitude_stabilization(void);
vector3d thrust_wrt_world(void);
void autopilot(void);
void numerical_dynamics(void);
void initialize_simulation(void);
void update_lander_state(void);
void reset_simulation(void);
vector3d axis_angle_rotation(vector3d v, vector3d k, double angle);
double lowest_radius_of_orbit(vector3d position, vector3d velocity);
double angle_between_vectors(vector3d x, vector3d y);
vector3d atmosphere_velocity(vector3d position);
void velocity_account_for_surface(void);
vector3d wind_velocity(vector3d position);
vector3d position_relative_to_surface(vector3d position);
void initialise_program(void);




UCLASS()
class TESTLANDER2_API ATestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATestActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Setup a pointer in the 'Simulation' blueprint version of TestActor to the explosion bluprint object, used to spawn it later
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	TSubclassOf<AActor> ExplosionPtr;

};








