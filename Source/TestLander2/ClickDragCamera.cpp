// Fill out your copyright notice in the Description page of Project Settings.


#include "ClickDragCamera.h"

AClickDragCamera::AClickDragCamera() {




}


void AClickDragCamera::ModifyCameraRotation(float mouseX, float mouseY, float strength) {
	camera_rot_x = camera_rot_x + mouseX * strength;
	camera_rot_y = FMath::Clamp(camera_rot_y + mouseY * strength, -89.9f, 89.9f);
}

void AClickDragCamera::ModifyCameraZoom(float mouseScrollAxis, float strength2) {
	camera_zoom = FMath::Clamp(camera_zoom + mouseScrollAxis * strength2, 200.0f, 3000.0f);
}