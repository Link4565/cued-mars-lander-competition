// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "WidgetBlueprint.h"
#include "Blueprint/UserWidget.h"
#include "TestActor.h"
#include "TestHUD.generated.h"

/**
 * 
 */
UCLASS()
class TESTLANDER2_API UTestHUD : public UUserWidget//UWidgetBlueprint
{
	GENERATED_BODY()
	
	//Decale functions to make global variables from the simulation visible to the HUD Blueprint

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetAltitude();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetClimbRate();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetGroundSpeed();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetSimulationSpeed();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetTime();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	FVector GetPosition();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	FVector GetVelocity();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetThrottle();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetThrust();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetFuel();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	FString GetScenario();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetAutopilotEnabled();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetAutopilotActive();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetAutopilotMode();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetAttitudeStabilizerEnabled();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetAttitudeStabilizerMode();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetAttitudeStabilizerAngle();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetParachuteStatus();

};
