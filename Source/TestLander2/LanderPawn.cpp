// Fill out your copyright notice in the Description page of Project Settings.


#include "LanderPawn.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ALanderPawn::ALanderPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create components that make up the lander
	Scene = CreateDefaultSubobject<USceneComponent>("Scene");
	Lander = CreateDefaultSubobject<UStaticMeshComponent>("Lander");
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Parachute = CreateDefaultSubobject<UStaticMeshComponent>("Parachute");
	Thruster = CreateDefaultSubobject<UParticleSystemComponent>("Thruster");

	//Set up the hirearchy of components within the lander
	RootComponent = Scene;
	Lander->SetupAttachment(RootComponent);
	Parachute->SetupAttachment(RootComponent);
	SpringArm->SetupAttachment(RootComponent);
	Camera->SetupAttachment(SpringArm);
	Thruster->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ALanderPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALanderPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALanderPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Throttle Up", IE_Pressed, this, &ALanderPawn::throttle_up);
	PlayerInputComponent->BindAction("Throttle Down", IE_Pressed, this, &ALanderPawn::throttle_down);
	PlayerInputComponent->BindAction("Faster Simulation", IE_Pressed, this, &ALanderPawn::faster_simulation);
	PlayerInputComponent->BindAction("Slower Simulation", IE_Pressed, this, &ALanderPawn::slower_simulation);
	PlayerInputComponent->BindAction("Exit", IE_Pressed, this, &ALanderPawn::exit);
	PlayerInputComponent->BindAction("Scenario 0", IE_Pressed, this, &ALanderPawn::scenario_0);
	PlayerInputComponent->BindAction("Scenario 1", IE_Pressed, this, &ALanderPawn::scenario_1);
	PlayerInputComponent->BindAction("Scenario 2", IE_Pressed, this, &ALanderPawn::scenario_2);
	PlayerInputComponent->BindAction("Scenario 3", IE_Pressed, this, &ALanderPawn::scenario_3);
	PlayerInputComponent->BindAction("Scenario 4", IE_Pressed, this, &ALanderPawn::scenario_4);
	PlayerInputComponent->BindAction("Scenario 5", IE_Pressed, this, &ALanderPawn::scenario_5);
	PlayerInputComponent->BindAction("Scenario 6", IE_Pressed, this, &ALanderPawn::scenario_6);
	PlayerInputComponent->BindAction("Toggle Autopilot", IE_Pressed, this, &ALanderPawn::toggle_autopilot);
	PlayerInputComponent->BindAction("Help", IE_Pressed, this, &ALanderPawn::toggle_help);
	PlayerInputComponent->BindAction("Lighting", IE_Pressed, this, &ALanderPawn::lighting);
	PlayerInputComponent->BindAction("Texture", IE_Pressed, this, &ALanderPawn::texture);
	PlayerInputComponent->BindAction("Deploy Parachute", IE_Pressed, this, &ALanderPawn::deploy_parachute);
	PlayerInputComponent->BindAction("Toggle Attitude Stabilizer", IE_Pressed, this, &ALanderPawn::toggle_attitude_stabilizer);
	PlayerInputComponent->BindAction("Space", IE_Pressed, this, &ALanderPawn::space);
	PlayerInputComponent->BindAction("Increase Attitude", IE_Pressed, this, &ALanderPawn::increase_attitude);
	PlayerInputComponent->BindAction("Decrease Attitude", IE_Pressed, this, &ALanderPawn::decrease_attitude);
	PlayerInputComponent->BindAction("Reset Attitude", IE_Pressed, this, &ALanderPawn::reset_attitude);
	PlayerInputComponent->BindAction("Toggle Autopilot Mode", IE_Pressed, this, &ALanderPawn::toggle_autopilot_mode);
	PlayerInputComponent->BindAction("Autopilot Activate", IE_Pressed, this, &ALanderPawn::autopilot_activate);

}

void ALanderPawn::throttle_up(void) {
	if (!autopilot_enabled && !landed && (fuel > 0.0) && !help) {
		throttle_control++;
		if (throttle_control > THROTTLE_GRANULARITY) throttle_control = THROTTLE_GRANULARITY;
		throttle = (double)throttle_control / THROTTLE_GRANULARITY;
	}
}

void ALanderPawn::throttle_down(void) {
	if (!autopilot_enabled && !landed && !help) {
		throttle_control--;
		if (throttle_control < 0) throttle_control = 0;
		throttle = (double)throttle_control / THROTTLE_GRANULARITY;
	}
}

void ALanderPawn::faster_simulation(void) {
	if (!help) {
		frames_until_update = 0;
		simulation_speed++;
		if (simulation_speed > 10) simulation_speed = 10;
		if (paused) {
			paused = false;
		}
	}
}

void ALanderPawn::slower_simulation(void) {
	if (!help) {
		frames_until_update = 0;
		simulation_speed--;
		if (simulation_speed < 0) simulation_speed = 0;
		if (!simulation_speed) {
			paused = true;
		}
	}
}

void ALanderPawn::exit(void) {
	UKismetSystemLibrary::QuitGame(GetWorld(), GetWorld()->GetFirstPlayerController(), EQuitPreference::Quit, false);
}

void ALanderPawn::scenario_0(void) {
	if (!help) {
		scenario = 0;
		reset_simulation();
	}
}

void ALanderPawn::scenario_1(void) {
	if (!help) {
		scenario = 1;
		reset_simulation();
	}
}

void ALanderPawn::scenario_2(void) {
	if (!help) {
		scenario = 2;
		reset_simulation();
	}
}

void ALanderPawn::scenario_3(void) {
	if (!help) {
		scenario = 3;
		reset_simulation();
	}
}

void ALanderPawn::scenario_4(void) {
	if (!help) {
		scenario = 4;
		reset_simulation();
	}
}

void ALanderPawn::scenario_5(void) {
	if (!help) {
		scenario = 5;
		reset_simulation();
	}
}

void ALanderPawn::scenario_6(void) {
	if (!help) {
		scenario = 6;
		reset_simulation();
	}
}

void ALanderPawn::toggle_autopilot(void) {
	if (!help) {
		autopilot_enabled = !autopilot_enabled;
		autopilot_active = false;
	}
}

void ALanderPawn::toggle_help(void) {
	if (help) {
		help = false;
		simulation_speed = saved_simulation_speed;
		if (simulation_speed == 0) paused = true;
		else paused = false;
	}
	else {
		help = true;
		saved_simulation_speed = simulation_speed;
		simulation_speed = 0;
		paused = true;
	}
}

void ALanderPawn::lighting(void) {
	//No functionality currently defined

	if (!help) {
		static_lighting = !static_lighting;
	}
}

void ALanderPawn::texture(void) {
	//No functionality currently defined

	if (!help) {
		do_texture = !do_texture;
	}
}

void ALanderPawn::deploy_parachute(void) {
	if (!autopilot_enabled && !landed && (parachute_status == NOT_DEPLOYED) && !help) parachute_status = DEPLOYED;
}

void ALanderPawn::toggle_attitude_stabilizer(void) {
	if (!autopilot_enabled && !landed && !help) stabilized_attitude = !stabilized_attitude;
}

void ALanderPawn::space(void) {
	if (!help) {
		simulation_speed = 0;
		if (paused && !landed) simulation_speed = 11;
		paused = true;
	}
}

void ALanderPawn::increase_attitude(void) {
	if (!autopilot_enabled && !landed && !help) {
		stabilized_attitude_angle_lat += PI / 20;
		if (stabilized_attitude_angle_lat > PI - SMALL_NUM) stabilized_attitude_angle_lat = -PI;
	}
}

void ALanderPawn::decrease_attitude(void) {
	if (!autopilot_enabled && !landed && !help) {
		stabilized_attitude_angle_lat -= PI / 20;
		if (stabilized_attitude_angle_lat < -PI + SMALL_NUM) stabilized_attitude_angle_lat = PI;
	}
}

void ALanderPawn::reset_attitude(void) {
	if (!autopilot_enabled && !landed && !help) stabilized_attitude_angle_lat = 0;
}

void ALanderPawn::toggle_autopilot_mode(void) {
	if (!autopilot_active && !help) {
		if (autopilot_mode != TAKE_OFF) {
			autopilot_mode = static_cast<autopilot_mode_t>(static_cast<int>(autopilot_mode) + 1);
		}
		else autopilot_mode = LAND;
	}
}

void ALanderPawn::autopilot_activate(void) {
	if (autopilot_enabled && !autopilot_active && !help) autopilot_active = true;
}
