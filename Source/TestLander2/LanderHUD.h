// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TestActor.h"
#include "LanderHUD.generated.h"

/**
 * 
 */
UCLASS()
class TESTLANDER2_API ULanderHUD : public UUserWidget
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetAltitude();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetAltitudeOrder();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetAltitudeAngle();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetClimbRate();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetClimbRateOrder();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetClimbRateAngle();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetGroundSpeed();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetGroundSpeedOrder();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetGroundSpeedAngle();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetSimulationSpeed();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetTime();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	FVector GetPosition();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	FVector GetVelocity();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetThrottle();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetThrust();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetFuel();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetFuelLitres();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	FString GetScenario();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetAutopilotEnabled();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetAutopilotActive();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetAutopilotMode();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetAttitudeStabilizerEnabled();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetAttitudeStabilizerMode();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	float GetAttitudeStabilizerAngle();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	int GetParachuteStatus();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetPaused();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetHelp();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetLanded();

	UFUNCTION(BlueprintCallable, Category = GlobalVariables)
	bool GetCrashed();

};
