// Fill out your copyright notice in the Description page of Project Settings.


#include "Mars.h"

// Sets default values
AMars::AMars()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//Set up structure of mars model made of many pieces

	Mars = CreateDefaultSubobject<USceneComponent>("Mars Scene");
	Mars1 = CreateDefaultSubobject<UStaticMeshComponent>("Mars1");
	Mars2 = CreateDefaultSubobject<UStaticMeshComponent>("Mars2");
	Mars3 = CreateDefaultSubobject<UStaticMeshComponent>("Mars3");
	Mars4 = CreateDefaultSubobject<UStaticMeshComponent>("Mars4");
	Mars5 = CreateDefaultSubobject<UStaticMeshComponent>("Mars5");
	Mars6 = CreateDefaultSubobject<UStaticMeshComponent>("Mars6");
	Mars7 = CreateDefaultSubobject<UStaticMeshComponent>("Mars7");
	Mars8 = CreateDefaultSubobject<UStaticMeshComponent>("Mars8");
	NorthPoleCap = CreateDefaultSubobject<UStaticMeshComponent>("NorthPoleCap");
	SouthPoleCap = CreateDefaultSubobject<UStaticMeshComponent>("SouthPoleCap");

	RootComponent = Mars;
	Mars1->SetupAttachment(RootComponent);
	Mars2->SetupAttachment(RootComponent);
	Mars3->SetupAttachment(RootComponent);
	Mars4->SetupAttachment(RootComponent);
	Mars5->SetupAttachment(RootComponent);
	Mars6->SetupAttachment(RootComponent);
	Mars7->SetupAttachment(RootComponent);
	Mars8->SetupAttachment(RootComponent);
	NorthPoleCap->SetupAttachment(RootComponent);
	SouthPoleCap->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AMars::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMars::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

