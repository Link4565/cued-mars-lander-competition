// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "TestActor.h"
#include "LanderPawn.generated.h"

UCLASS()
class TESTLANDER2_API ALanderPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ALanderPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USceneComponent* Scene;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Lander;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Parachute;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UParticleSystemComponent* Thruster;

	//Input Functions
	void throttle_up(void);
	void throttle_down(void);
	void faster_simulation(void);
	void slower_simulation(void);
	void exit(void);
	void scenario_0(void);
	void scenario_1(void);
	void scenario_2(void);
	void scenario_3(void);
	void scenario_4(void);
	void scenario_5(void);
	void scenario_6(void);
	void toggle_autopilot(void);
	void toggle_help(void);
	void lighting(void);
	void texture(void);
	void deploy_parachute(void);
	void toggle_attitude_stabilizer(void);
	void space(void);
	void increase_attitude(void);
	void decrease_attitude(void);
	void reset_attitude(void);
	void toggle_autopilot_mode(void);
	void autopilot_activate(void);

};
