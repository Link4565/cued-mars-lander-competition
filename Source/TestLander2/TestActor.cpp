// Fill out your copyright notice in the Description page of Project Settings.


#define DECLARE_GLOBAL_VARIABLES
#include "TestActor.h"
#include "FastNoiseWrapper.h"
#include "GameFramework\Controller.h"
#include "DrawDebugHelpers.h"
#include "Components\PointLightComponent.h"
#include "GameFramework\SpringArmComponent.h"
#include "Particles\ParticleSystemComponent.h"


// Sets default values
ATestActor::ATestActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    //Setup simplex noise used to generate 'random' wind
    wind_gusts_n = CreateDefaultSubobject<UFastNoiseWrapper>(TEXT("wind_gusts_n"));
    wind_gusts_e = CreateDefaultSubobject<UFastNoiseWrapper>(TEXT("wind_gusts_e"));
    wind_gusts_n->SetupFastNoise(EFastNoise_NoiseType::Simplex, 1337, 0.0001);
    wind_gusts_e->SetupFastNoise(EFastNoise_NoiseType::Simplex, 1337, 0.0001);

}

// Called when the game starts or when spawned
void ATestActor::BeginPlay()
{
	Super::BeginPlay();
    
    //Setup a pointer to Mars in order to control and reference it
    for (TObjectIterator<AActor> Itr; Itr; ++Itr)
    {
        if (Itr->Tags.Contains("Mars"))
        {
            mars_pointer = *Itr;
        }
        else
        {
            continue;
        }
    }

    //Setup a pointer to the lander
    player = GetWorld()->GetFirstPlayerController()->GetPawn();
    
    initialise_program();

}

// Called every frame
void ATestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    FHitResult Hit(ForceInit);
    FVector End;
    FCollisionQueryParams CollisionParams;
    FVector Start;

    int repeats;


    vector3d closeup_position, closeup_drag_force;
    FVector f_closeup_position;

    //Simulation control code
    if (frames_until_update > 0)
    //Count down number of frames until next update
    {
        frames_until_update--;
    }
    else {
        switch (simulation_speed)
        //Set the number of updates to run in this frame, and the delay until the next update, based on the current simulation speed
        {
        case 0:
            frames_until_update = 0;
            repeats = 0;
            break;
        case 1:
            frames_until_update = 10;
            repeats = 1;
            break;
        case 2:
            frames_until_update = 6;
            repeats = 1;
            break;
        case 3:
            frames_until_update = 4;
            repeats = 1;
            break;
        case 4:
            frames_until_update = 2;
            repeats = 1;
            break;
        case 5:
            frames_until_update = 0;
            repeats = 1;
            break;
        case 6:
            frames_until_update = 0;
            repeats = 10;
            break;
        case 7:
            frames_until_update = 0;
            repeats = 40;
            break;
        case 8:
            frames_until_update = 0;
            repeats = 100;
            break;
        case 9:
            frames_until_update = 0;
            repeats = 300;
            break;
        case 10:
            frames_until_update = 0;
            repeats = 1000;
            break;
        case 11:
            simulation_speed = 0;
            frames_until_update = 0;
            repeats = 1;
            break;
        }

        //Run the simulation 'repeats' times in the current frame
        for (int i = 0; i < repeats; i++) {
            //Only run the simulation if the lander hasn't landed
            if (!landed) {
                //To reduce the impact of single precision floating point errors in positioning and rotation, position mars and the lander differently depending on altitude
                if (altitude < 1000) {
                    //If near the surface of Mars, lock the position and rotation of Mars, and move the lander towards the surface, such that the
                    //lander always stays very close to the origin, and such errors in the position of Mars are fixed during the descent to prevent vibration.
                    if (surface_approach_location == vector3d(0, 0, 0)) {
                        surface_approach_location = position;
                        surface_approach_rotation = mars_rotation;
                    }
                    closeup_position = axis_angle_rotation(position, vector3d(0, 0, 1), FMath::DegreesToRadians(surface_approach_rotation - mars_rotation));
                    f_closeup_position = FVector(static_cast<float>(-surface_approach_location.x + closeup_position.x), static_cast<float>(surface_approach_location.y - closeup_position.y), static_cast<float>(-surface_approach_location.z + closeup_position.z));
                    player->SetActorLocation(10 * f_closeup_position);
                    mars_pointer->SetActorLocation(10 * FVector(static_cast<float>(-surface_approach_location.x), static_cast<float>(surface_approach_location.y), static_cast<float>(-surface_approach_location.z)));
                    mars_pointer->SetActorRotation(FRotator(180.0f, 0.0f, 0.0f));
                    mars_pointer->AddActorWorldRotation(FRotator(0.0f, -surface_approach_rotation, 0.0f));
                    closeup_drag_force = axis_angle_rotation(drag_force, vector3d(0, 0, 1), FMath::DegreesToRadians(surface_approach_rotation - mars_rotation));
                }
                else {
                    //When far above the surface of Mars, keep the lander at the origin and move Mars around it, to prevent vibration in the position of the lander and camera due to the large distances
                    surface_approach_location = vector3d(0, 0, 0);
                    surface_approach_rotation = mars_rotation;
                    closeup_position = position;
                    closeup_drag_force = drag_force;
                    player->SetActorLocation(10 * FVector(0, 0, 0));
                    mars_pointer->SetActorLocation(10 * FVector(static_cast<float>(-position.x), static_cast<float>(position.y), static_cast<float>(-position.z)));
                    mars_pointer->SetActorRotation(FRotator(180.0f, 0.0f, 0.0f));
                    mars_pointer->AddActorWorldRotation(FRotator(0.0f, -mars_rotation, 0.0f));
                }

                //Trace a line from the lander to the centre of Mars, checking for an impact with Mars' surface
                Hit = FHitResult(ForceInit);
                End = mars_pointer->GetActorLocation();
                Start = player->GetActorLocation();
                GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_GameTraceChannel1, CollisionParams);
                if (Hit.Location != FVector(0.0, 0.0, 0.0))
                //Ignore invalid results
                {
                    //If the surface of Mars is not found, assume lander is beneath the surface, otherwise record the altitude and current radius of Mars
                    if (Hit.Location == End) altitude = 0;
                    else altitude = Hit.Distance / 10;
                    current_radius = position.toFVector().Size() / 1 - altitude; 
                }
                //Lines to visualise 'laser altimeter' and point of contact with the surface
                /*
                //FlushPersistentDebugLines(GetWorld());
                //DrawDebugLine(GetWorld(), Start, End, FColor::Green, true, -1, 0, 1);
                //DrawDebugPoint(GetWorld(), Hit.Location, 20, FColor(52, 220, 239), true);
                */

                //Update the simulation by one step
                update_lander_state();
                //Log position over time to test for errors at higher simulation speeds
                //if (scenario == 1) UE_LOG(LogTemp, Warning, TEXT("Text, %f"), static_cast<float>(altitude));
            }
        }

        /* Old Movement Script (For future reference)
        for (int i = 0; i < repeats; i++) {
            if (!landed) {
                Hit = FHitResult(ForceInit);
                End = FVector(0.0f, 0.0f, 0.0f);
                Start = (1 * FVector(static_cast<float>(position.x), static_cast<float>(-position.y), static_cast<float>(-position.z)));//.RotateAngleAxis(i * 360 * (delta_t / MARS_DAY), FVector(0, 0, 1));
                Start = Start.RotateAngleAxis(360 * (simulation_time / MARS_DAY), FVector(0, 0, 1));
                GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_GameTraceChannel1, CollisionParams);
                if (Hit.Distance == Start.Size()) altitude = 0;
                else altitude = Hit.Distance / 1;
                current_radius = Start.Size() / 1 - altitude;
                FlushPersistentDebugLines(GetWorld());
                DrawDebugLine(GetWorld(), Start, End, FColor::Green, true, -1, 0, 1);
                DrawDebugPoint(GetWorld(), Hit.Location, 20, FColor(52, 220, 239), true);

                if (altitude < 1000) {
                    if (surface_approach_location == vector3d(0, 0, 0)) {
                        surface_approach_location = position;
                        surface_approach_rotation = mars_rotation;
                    }
                }
                else {
                    surface_approach_location = vector3d(0, 0, 0);
                    surface_approach_rotation = mars_rotation;
                }

                update_lander_state();
                //if (scenario == 1) UE_LOG(LogTemp, Warning, TEXT("Text, %f"), static_cast<float>(altitude));
            }
        }
        */
    }

    // Onscreen debug messages code (used to give variable readouts at runtime)
    //if (GEngine) {
        //GEngine->ClearOnScreenDebugMessages();
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Lander Position: Scenario = %f, %f, %f : %f"), static_cast<float>(position.x), static_cast<float>(position.y), static_cast<float>(position.z), static_cast<float>(scenario)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Lander Position Render = %f, %f, %f"), static_cast<float>(player->GetActorLocation().X), static_cast<float>(player->GetActorLocation().Y), static_cast<float>(player->GetActorLocation().Z)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Surface Ground Speed Lat: %f"), static_cast<float>(surface_ground_speed_lat)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Surface Ground Speed Long: %f"), static_cast<float>(surface_ground_speed_long)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Stabilized Attitude Angle Lat: %f"), static_cast<float>(stabilized_attitude_angle_lat)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Stabilized Attitude Angle Long: %f"), static_cast<float>(stabilized_attitude_angle_long)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Stabilized Attitude Angle Mode: %f"), static_cast<float>(attitude_stabilizer_mode)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Lander Orientation = %f, %f, %f"), static_cast<float>(orientation.x), static_cast<float>(orientation.y), static_cast<float>(orientation.z)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("thrust_wrt_world() = %f, %f, %f"), static_cast<float>(thrust_wrt_world().x), static_cast<float>(thrust_wrt_world().y), static_cast<float>(thrust_wrt_world().z)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Altitude = %f: %f %f %f"), static_cast<float>(altitude), static_cast<float>(Hit.Location.X), static_cast<float>(Hit.Location.Y), static_cast<float>(Hit.Location.Z)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Landed = %f"), static_cast<float>(landed)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Autopilot Enabled = %f"), static_cast<float>(autopilot_enabled)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Autopilot Active = %f"), static_cast<float>(autopilot_active)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Simulation Speed = %f"), static_cast<float>(simulation_speed)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Frames Until Update = %f"), static_cast<float>(frames_until_update)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Camera Zoom = %f"), static_cast<float>(camera_zoom)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Drag Force = %f,%f,%f"), static_cast<float>(drag_force.x), static_cast<float>(drag_force.y), static_cast<float>(drag_force.z)));
        //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Drag Force FVector = %f,%f,%f"), drag_force.toFVector().X, drag_force.toFVector().Y, drag_force.toFVector().Z));
    //}
    
    /* Old Positioning code (For future reference)
    if (altitude < 1000) {
        //if (surface_approach_location == vector3d(0, 0, 0)) {
        //    surface_approach_location = position;
        //    surface_approach_rotation = mars_rotation;
        //}
        closeup_position = axis_angle_rotation(position, vector3d(0, 0, 1), FMath::DegreesToRadians(surface_approach_rotation - mars_rotation));
        f_closeup_position = FVector(static_cast<float>(-surface_approach_location.x + closeup_position.x), static_cast<float>(surface_approach_location.y - closeup_position.y), static_cast<float>(surface_approach_location.z - closeup_position.z));
        player->SetActorLocation(1*f_closeup_position);
        mars_pointer->SetActorLocation(1*FVector(static_cast<float>(-surface_approach_location.x), static_cast<float>(surface_approach_location.y), static_cast<float>(surface_approach_location.z)));
        //mars_pointer->SetActorRotation(FRotator(0.0f, -surface_approach_rotation, 0.0f));
        mars_pointer->SetActorRotation(FRotator(180.0f, 0.0f, 0.0f));
        //mars_pointer->AddActorWorldRotation(FRotator(0.0f, 180.0f, 0.0f));
        mars_pointer->AddActorWorldRotation(FRotator(0.0f, -surface_approach_rotation, 0.0f));
        closeup_drag_force = axis_angle_rotation(drag_force, vector3d(0, 0, 1), FMath::DegreesToRadians(surface_approach_rotation - mars_rotation));
    }
    else {
        //surface_approach_location = vector3d(0, 0, 0);
        //surface_approach_rotation = mars_rotation;
        closeup_position = position;
        closeup_drag_force = drag_force;
        player->SetActorLocation(1*FVector(0, 0, 0));
        mars_pointer->SetActorLocation(1*FVector(static_cast<float>(-position.x), static_cast<float>(position.y), static_cast<float>(position.z)));
        //mars_pointer->SetActorRotation(FRotator(0.0f, -mars_rotation, 0.0f));
        mars_pointer->SetActorRotation(FRotator(180.0f, 0.0f, 0.0f));
        //mars_pointer->AddActorWorldRotation(FRotator(0.0f, 180.0f, 0.0f));
        mars_pointer->AddActorWorldRotation(FRotator(0.0f, -mars_rotation, 0.0f));
    }
    */

    //Run Mars and lander positioning code after the last simulation step and during frames without a simulation step to show the latest positions
    if (altitude < 1000) {
        if (surface_approach_location == vector3d(0, 0, 0)) {
            surface_approach_location = position;
            surface_approach_rotation = mars_rotation;
        }
        closeup_position = axis_angle_rotation(position, vector3d(0, 0, 1), FMath::DegreesToRadians(surface_approach_rotation - mars_rotation));
        f_closeup_position = FVector(static_cast<float>(-surface_approach_location.x + closeup_position.x), static_cast<float>(surface_approach_location.y - closeup_position.y), static_cast<float>(-surface_approach_location.z + closeup_position.z));
        player->SetActorLocation(10 * f_closeup_position);
        mars_pointer->SetActorLocation(10 * FVector(static_cast<float>(-surface_approach_location.x), static_cast<float>(surface_approach_location.y), static_cast<float>(-surface_approach_location.z)));
        mars_pointer->SetActorRotation(FRotator(180.0f, 0.0f, 0.0f));
        mars_pointer->AddActorWorldRotation(FRotator(0.0f, -surface_approach_rotation, 0.0f));
        closeup_drag_force = axis_angle_rotation(drag_force, vector3d(0, 0, 1), FMath::DegreesToRadians(surface_approach_rotation - mars_rotation));
    }
    else {
        surface_approach_location = vector3d(0, 0, 0);
        surface_approach_rotation = mars_rotation;
        closeup_position = position;
        closeup_drag_force = drag_force;
        player->SetActorLocation(10 * FVector(0, 0, 0));
        mars_pointer->SetActorLocation(10 * FVector(static_cast<float>(-position.x), static_cast<float>(position.y), static_cast<float>(-position.z)));
        mars_pointer->SetActorRotation(FRotator(180.0f, 0.0f, 0.0f));
        mars_pointer->AddActorWorldRotation(FRotator(0.0f, -mars_rotation, 0.0f));
    }

    //Point the camera at the lander, with 'up' away from the centre of Mars
    player->SetActorRotation(FVector(static_cast<float>(closeup_position.x), static_cast<float>(-closeup_position.y), static_cast<float>(closeup_position.z)).Rotation());
    player->AddActorLocalRotation(FRotator(-90.1f, 0.0f, 0.0f));
    
    //Remove rapid/instantaneous 180 degree rotation of the camera caused by passing over the poles
    //Only run if the camera has already been moved once
    if (previous_camera_rotation != vector3d(999, 999, 999)) {
        //Test if near the north or south poles
        if (FMath::RadiansToDegrees(angle_between_vectors(vector3d(0,0,1),vector3d(position.x,position.y,abs(position.z)))) < 45) {
            //Test if the camera has rotated significantly in the last frame
            if (FMath::RadiansToDegrees(angle_between_vectors(vector3d(FVector::VectorPlaneProject(player->GetActorRotation().Vector(), FVector(0, 0, 1))), previous_camera_rotation)) > 170) {
                //Rotate the camera back to point in the previous direction
                camera_rot_x += 180;
            }
        }
    }
    
    //Record this frame's camera orientation
    previous_camera_rotation = vector3d(FVector::VectorPlaneProject(player->GetActorRotation().Vector(), FVector(0, 0, 1)));
    
    //Rotate the camera about the lander, keeping 'up' away from the centre of Mars
    player->AddActorLocalRotation(FRotator(0.0f, camera_rot_x, 0.0f));
    player->AddActorLocalRotation(FRotator(camera_rot_y, 0.0f, 0.0f));
    
    //Iterate through all the 'StaticMesh' components of the lander
    TArray<UStaticMeshComponent*> StaticMeshes;
    player->GetComponents<UStaticMeshComponent>(StaticMeshes);
    for (UStaticMeshComponent* Mesh : StaticMeshes)
    {
        if (Mesh->GetName() == "Lander") {
            //Orient the lander model, accounting for both modes of lander positioning
            if (stabilized_attitude) {
                Mesh->SetWorldRotation(FVector(static_cast<float>(lander_up_vector.x), static_cast<float>(-lander_up_vector.y), static_cast<float>(lander_up_vector.z)).Rotation());
            }
            else {
                Mesh->SetWorldRotation(FRotator(FQuat(FRotator(static_cast<float>(-orientation.x), static_cast<float>(orientation.y), static_cast<float>(orientation.z))) * FQuat(FRotator(0.0f, surface_approach_rotation - mars_rotation, 0.0f))));
            }
        }
        else if (Mesh->GetName() == "Parachute") {
            //Orient the parachute
            Mesh->SetWorldRotation(FVector(static_cast<float>(closeup_drag_force.x), static_cast<float>(-closeup_drag_force.y), static_cast<float>(closeup_drag_force.z)).Rotation());
            //Show the parachute if deployed
            if (parachute_status == DEPLOYED) {
                Mesh->SetVisibility(true);
            }
            else {
                Mesh->SetVisibility(false);
            }
        }
    }

    //Iterate through the 'ParticleSystem' components of the lander
    TArray<UParticleSystemComponent*> ParticleSystems;
    player->GetComponents<UParticleSystemComponent>(ParticleSystems);
    for (UParticleSystemComponent* Particle : ParticleSystems)
    {
        if (Particle->GetName() == "Thruster") {
            //Orient the thruster to point out of the bottom of the lander
            if (stabilized_attitude) {
                Particle->SetWorldRotation(FVector(static_cast<float>(lander_up_vector.x), static_cast<float>(-lander_up_vector.y), static_cast<float>(lander_up_vector.z)).Rotation());
            }
            else {
                Particle->SetWorldRotation(FRotator(FQuat(FRotator(static_cast<float>(-orientation.x), static_cast<float>(orientation.y), static_cast<float>(orientation.z))) * FQuat(FRotator(0.0f, surface_approach_rotation - mars_rotation, 0.0f))));
            }
            Particle->AddLocalRotation(FRotator(90.0f, 0.0f, 0.0f));
            //Set the length of the flames to match the throttle position
            Particle->SetFloatParameter("Lifetime", throttle * 2);
            //Disable the flames when the throttle is 0 to prevent flickering
            if (throttle == 0) {
                Particle->DeactivateSystem();
            }
            else if (!(Particle->IsActive())){
                Particle->ActivateSystem();
            }
        }
    }

    //Find the 'PointLight' component of the lander, used to create the glow of the thruster flames
    UPointLightComponent* flame_light = static_cast<UPointLightComponent*>(player->GetComponentByClass(UPointLightComponent::StaticClass()));
    //Set the light intensity to match the throttle position
    flame_light->Intensity = 3000000 * throttle;
    //Disable the light when turned off to prevent unwanted bloom and reflections
    if (throttle == 0) flame_light->SetVisibility(false);
    else if (!(flame_light->GetVisibleFlag())) flame_light->SetVisibility(true);
        
    //Set the distance of the camera from the lander to create the zoom effect
    static_cast<USpringArmComponent*>(player->GetComponentByClass(USpringArmComponent::StaticClass()))->TargetArmLength = camera_zoom;

    //Create an explosion at the lander when crashed
    if (crashed && !exploded) {
        exploded = true;
        FActorSpawnParameters SpawnParams;
        AActor* Explosion = GetWorld()->SpawnActor<AActor>(ExplosionPtr, player->GetActorLocation(), FRotator(FQuat(FRotator(static_cast<float>(-orientation.y), static_cast<float>(orientation.x), static_cast<float>(orientation.z))) * FQuat(FRotator(0.0f, surface_approach_rotation - mars_rotation, 0.0f))), SpawnParams);
    }
}


void xyz_euler_to_matrix(vector3d ang, double m[])
// Constructs a 4x4 OpenGL rotation matrix from xyz Euler angles
{
    double sin_a, sin_b, sin_g, cos_a, cos_b, cos_g;
    double ra, rb, rg;

    // Pre-calculate radian angles
    ra = ang.x * PI / (double)180;
    rb = ang.y * PI / (double)180;
    rg = ang.z * PI / (double)180;

    // Pre-calculate sines and cosines
    cos_a = cos(ra);
    cos_b = cos(rb);
    cos_g = cos(rg);
    sin_a = sin(ra);
    sin_b = sin(rb);
    sin_g = sin(rg);

    // Create the correct matrix coefficients
    m[0] = cos_a * cos_b;
    m[1] = sin_a * cos_b;
    m[2] = -sin_b;
    m[3] = 0.0;
    m[4] = cos_a * sin_b * sin_g - sin_a * cos_g;
    m[5] = sin_a * sin_b * sin_g + cos_a * cos_g;
    m[6] = cos_b * sin_g;
    m[7] = 0.0;
    m[8] = cos_a * sin_b * cos_g + sin_a * sin_g;
    m[9] = sin_a * sin_b * cos_g - cos_a * sin_g;
    m[10] = cos_b * cos_g;
    m[11] = 0.0;
    m[12] = 0.0;
    m[13] = 0.0;
    m[14] = 0.0;
    m[15] = 1.0;
}

vector3d matrix_to_xyz_euler(double m[])
// Decomposes a 4x4 OpenGL rotation matrix into xyz Euler angles
{
    double tmp;
    vector3d ang;

    // Catch degenerate elevation cases
    if (m[2] < -0.99999999) {
        ang.y = 90.0;
        ang.x = 0.0;
        ang.z = acos(m[8]);
        if ((sin(ang.z) > 0.0) ^ (m[4] > 0.0)) ang.z = -ang.z;
        ang.z *= 180.0 / PI;
        return ang;
    }
    if (m[2] > 0.99999999) {
        ang.y = -90.0;
        ang.x = 0.0;
        ang.z = acos(m[5]);
        if ((sin(ang.z) < 0.0) ^ (m[4] > 0.0)) ang.z = -ang.z;
        ang.z *= 180.0 / PI;
        return ang;
    }

    // Non-degenerate elevation - between -90 and +90
    ang.y = asin(-m[2]);

    // Now work out azimuth - between -180 and +180
    tmp = m[0] / cos(ang.y); // the denominator will not be zero
    if (tmp <= -1.0) ang.x = PI;
    else if (tmp >= 1.0) ang.x = 0.0;
    else ang.x = acos(tmp);
    if (((sin(ang.x) * cos(ang.y)) > 0.0) ^ ((m[1]) > 0.0)) ang.x = -ang.x;

    // Now work out roll - between -180 and +180
    tmp = m[10] / cos(ang.y); // the denominator will not be zero
    if (tmp <= -1.0) ang.z = PI;
    else if (tmp >= 1.0) ang.z = 0.0;
    else ang.z = acos(tmp);
    if (((sin(ang.z) * cos(ang.y)) > 0.0) ^ ((m[6]) > 0.0)) ang.z = -ang.z;

    // Convert to degrees
    ang.y *= 180.0 / PI;
    ang.x *= 180.0 / PI;
    ang.z *= 180.0 / PI;

    return ang;
}

void microsecond_time(unsigned long long& t)
// Returns system time in microseconds
{
    t = (FDateTime::Now() - FDateTime::MinValue()).GetTotalMicroseconds();
}

double atmospheric_density(vector3d pos)
// Simple exponential model between surface and exosphere (around 200km), surface density is approximately 0.017 kg/m^3,
// scale height is approximately 11km
{
    double alt;

    alt = pos.abs() - current_radius;
    if ((alt > EXOSPHERE) || (alt < 0.0)) return 0.0;
    else return (0.017 * exp(-alt / 11000.0));
}

void update_closeup_coords(void)
// Updates the close-up view's coordinate frame, based on the lander's current position and velocity.
// This needs to be called every time step, even if the view is not being rendered, since any-angle
// attitude stabilizers reference closeup_coords.right
{
    vector3d s, tv, t;
    double tmp;

    // Direction from surface to lander (radial) - this must map to the world y-axis
    s = position.norm();

    // Direction of tangential velocity - this must map to the world x-axis
    tv = velocity_from_positions - (velocity_from_positions * s) * s;
    if (tv.abs() < SMALL_NUM) // vertical motion only, use last recorded tangential velocity
        tv = closeup_coords.backwards ? (closeup_coords.right * s) * s - closeup_coords.right : closeup_coords.right - (closeup_coords.right * s) * s;
    if (tv.abs() > SMALL_NUM) t = tv.norm();

    // Check these two vectors are non-zero and perpendicular (they should be, unless s and closeup_coords.right happen to be parallel)
    if ((tv.abs() <= SMALL_NUM) || (fabs(s * t) > SMALL_NUM)) {
        // Set t to something perpendicular to s
        t.x = -s.y; t.y = s.x; t.z = 0.0;
        if (t.abs() < SMALL_NUM) { t.x = -s.z; t.y = 0.0; t.z = s.x; }
        t = t.norm();
    }

    // Adjust the terrain texture angle if the lander has changed direction. The motion will still be along
    // the x-axis, so we need to rotate the texture to compensate.
    if (closeup_coords.initialized) {
        if (closeup_coords.backwards) {
            tmp = (-closeup_coords.right) * t;
            if (tmp > 1.0) tmp = 1.0; if (tmp < -1.0) tmp = -1.0;
            if (((-closeup_coords.right ^ t) * position.norm()) < 0.0) terrain_angle += (180.0 / PI) * acos(tmp);
            else terrain_angle -= (180.0 / PI) * acos(tmp);
        }
        else {
            tmp = closeup_coords.right * t;
            if (tmp > 1.0) tmp = 1.0; if (tmp < -1.0) tmp = -1.0;
            if (((closeup_coords.right ^ t) * position.norm()) < 0.0) terrain_angle += (180.0 / PI) * acos(tmp);
            else terrain_angle -= (180.0 / PI) * acos(tmp);
        }
        while (terrain_angle < 0.0) terrain_angle += 360.0;
        while (terrain_angle >= 360.0) terrain_angle -= 360.0;
    }

    // Normally we maintain motion to the right, the one exception being when the ground speed passes
    // through zero and changes sign. A sudden 180 degree change of viewpoint would be confusing, so
    // in this instance we allow the lander to fly to the left.
    if (closeup_coords.initialized && ((closeup_coords.right * t) < 0.0)) {
        closeup_coords.backwards = true;
        closeup_coords.right = -1.0 * t;
    }
    else {
        closeup_coords.backwards = false;
        closeup_coords.right = t;
        closeup_coords.initialized = true;
    }
}

bool safe_to_deploy_parachute(void)
// Checks whether the parachute is safe to deploy at the current position and velocity
{
    double drag;

    // Assume high Reynolds number, quadratic drag = -0.5 * rho * v^2 * A * C_d
    drag = 0.5 * DRAG_COEF_CHUTE * atmospheric_density(position) * 5.0 * 2.0 * LANDER_SIZE * 2.0 * LANDER_SIZE * (velocity_from_positions - atmosphere_velocity(position)).abs2();
    // Do not use the global variable "altitude" here, in case this function is called from within the
    // numerical_dynamics function, before altitude is updated in the update_visualization function
    if ((drag > MAX_PARACHUTE_DRAG) || ((velocity_from_positions.abs() > MAX_PARACHUTE_SPEED) && ((position.abs() - current_radius) < EXOSPHERE))) return false;
    else return true;
}

void update_visualization(void)
// The visualization part of the idle function. Re-estimates altitude, velocity, climb speed and ground
// speed from current and previous positions. Updates throttle and fuel levels, then redraws all subwindows.
{
    static vector3d last_track_position;
    vector3d av_p, d;
    double a, b, c, mu;

    //Manually calculate the altitude against 'MARS_RADIUS' before the lander has been positioned and a reading of altitude taken
    if (simulation_time == 0.0) altitude = position.abs() - MARS_RADIUS;
    
    simulation_time += delta_t;
    //Increase the rotation of mars, keeping the angle in range 0,360
    mars_rotation = fmod((mars_rotation + 360 * (delta_t / MARS_DAY)), 360);

    // Use average of current and previous positions when calculating climb and ground speeds
    av_p = (position + last_position).norm();
    if (delta_t != 0.0) velocity_from_positions = (position - last_position) / delta_t;
    else velocity_from_positions = vector3d(0.0, 0.0, 0.0);
    climb_speed = velocity_from_positions * av_p;
    //Lander speed around mars, assuming Mars is not spinning
    static_ground_speed = (velocity_from_positions - climb_speed * av_p).abs();
    //Lander speed relative to the surface of mars
    surface_ground_speed = (velocity_from_positions - atmosphere_velocity(position) - climb_speed * av_p).abs();
    //Lander speed travelling north/south across the surface
    surface_ground_speed_lat = (-((velocity_from_positions - atmosphere_velocity(position) - climb_speed * av_p)) * ((vector3d(0, 0, 1) ^ position) ^ position).norm());
    //Lander speed travelling east/west across the surface
    surface_ground_speed_long = (((velocity_from_positions - atmosphere_velocity(position) - climb_speed * av_p)) * (vector3d(0, 0, 1) ^ position).norm());

    
    // Check to see whether the lander has landed
    if (position.abs() - LANDER_SIZE / 2.0 < current_radius || altitude == 0) {
        // Estimate position and time of impact
        
        d = position - last_position;
        a = d.abs2();
        b = 2.0 * last_position * d;
        c = last_position.abs2() - (current_radius + LANDER_SIZE / 2.0) * (current_radius + LANDER_SIZE / 2.0);
        mu = (-b - sqrt(b * b - 4.0 * a * c)) / (2.0 * a);
        position = last_position + mu * d;
        simulation_time -= (1.0 - mu) * delta_t;
        mars_rotation = fmod((mars_rotation - 360 * (((1.0 - mu) * delta_t) / MARS_DAY)), 360);
        altitude = LANDER_SIZE / 2.0;

        landed = true;
        if ((fabs(climb_speed) > MAX_IMPACT_DESCENT_RATE) || (fabs(surface_ground_speed) > MAX_IMPACT_GROUND_SPEED)) crashed = true;
        velocity_from_positions = vector3d(0.0, 0.0, 0.0);

    }
    

    // Update throttle and fuel (throttle might have been adjusted by the autopilot)
    if (throttle < 0.0) throttle = 0.0;
    if (throttle > 1.0) throttle = 1.0;
    fuel -= delta_t * (FUEL_RATE_AT_MAX_THRUST * throttle) / FUEL_CAPACITY;
    if (fuel <= 0.0) fuel = 0.0;
    if (landed || (fuel == 0.0)) throttle = 0.0;
    throttle_control = (short)(throttle * THROTTLE_GRANULARITY + 0.5);

    // Check to see whether the parachute has vaporized or the tethers have snapped
    if (parachute_status == DEPLOYED) {
        if (!safe_to_deploy_parachute() || parachute_lost) {
            parachute_lost = true; // to guard against the autopilot reinstating the parachute!
            parachute_status = LOST;
        }
    }

    // Update record of lander's previous positions, but only if the position or the velocity has 
    // changed significantly since the last update
    if (!track.n || ((position - last_track_position).norm() * velocity_from_positions.norm()) < TRACK_ANGLE_DELTA
        || (position - last_track_position).abs() > TRACK_DISTANCE_DELTA) {
        track.pos[track.p] = position;
        track.n++; if (track.n > N_TRACK) track.n = N_TRACK;
        track.p++; if (track.p == N_TRACK) track.p = 0;
        last_track_position = position;
    }

}

void attitude_stabilization(void)
// Three-axis stabilization to ensure the lander's base is always pointing downwards 
{
    vector3d up, left, out;
    double m[16];

    up = position.norm(); // this is the direction we want the lander's nose to point in

    // !!!!!!!!!!!!! HINT TO STUDENTS ATTEMPTING THE EXTENSION EXERCISES !!!!!!!!!!!!!!
    // For any-angle attitude control, we just need to set "up" to something different,
    // and leave the remainder of this function unchanged. For example, suppose we want
    // the attitude to be stabilized at stabilized_attitude_angle_lat to the vertical in the
    // close-up view. So we need to rotate "up" by stabilized_attitude_angle_lat degrees around
    // an axis perpendicular to the plane of the close-up view. This axis is given by the
    // vector product of "up"and "closeup_coords.right". To calculate the result of the
    // rotation, search the internet for information on the axis-angle rotation formula.

    vector3d rotation_axis_1, rotation_axis_2;

    switch (attitude_stabilizer_mode)
    //Allow stabilization in different modes
    {

    case ORBIT:
        //Rotation in plane of orbit
        rotation_axis_1 = (up ^ velocity).norm();
        rotation_axis_2 = vector3d(0, 0, 1);
        break;

    case CLOSE_UP:
        //Rotation in plane of close-up view
        rotation_axis_1 = closeup_coords.backwards ? (up ^ (-1 * closeup_coords.right)).norm() : (up ^ closeup_coords.right).norm();
        rotation_axis_2 = vector3d(0, 0, 1);
        break;

    case LAT_LONG:
        //Rotation along lines of lat-long
        rotation_axis_1 = (vector3d(0, 0, 1) ^ up).norm();
        rotation_axis_2 = (vector3d(0, 0, 1) ^ up ^ up).norm();
        break;
    }

    up = axis_angle_rotation(up, rotation_axis_1, stabilized_attitude_angle_lat);
    if (attitude_stabilizer_mode == LAT_LONG) up = axis_angle_rotation(up, rotation_axis_2, stabilized_attitude_angle_long);
    //Store a vector pointing up through the lander to use when positioning the lander onscreen
    lander_up_vector = up;

    // Set left to something perpendicular to up
    left.x = -up.y; left.y = up.x; left.z = 0.0;
    if (left.abs() < SMALL_NUM) { left.x = -up.z; left.y = 0.0; left.z = up.x; }
    left = left.norm();
    out = left ^ up;
    // Construct modelling matrix (rotation only) from these three vectors
    m[0] = out.x; m[1] = out.y; m[2] = out.z; m[3] = 0.0;
    m[4] = left.x; m[5] = left.y; m[6] = left.z; m[7] = 0.0;
    m[8] = up.x; m[9] = up.y; m[10] = up.z; m[11] = 0.0;
    m[12] = 0.0; m[13] = 0.0; m[14] = 0.0; m[15] = 1.0;
    // Decomponse into xyz Euler angles
    orientation = matrix_to_xyz_euler(m);
}

vector3d thrust_wrt_world(void)
// Works out thrust vector in the world reference frame, given the lander's orientation
{
    double m[16], k, delayed_throttle, lag = ENGINE_LAG;
    vector3d a, b;
    static double lagged_throttle = 0.0;
    static double last_time_lag_updated = -1.0;

    if (simulation_time < last_time_lag_updated) lagged_throttle = 0.0; // simulation restarted
    if (throttle < 0.0) throttle = 0.0;
    if (throttle > 1.0) throttle = 1.0;
    if (landed || (fuel == 0.0)) throttle = 0.0;

    if (simulation_time != last_time_lag_updated) {

        // Delayed throttle value from the throttle history buffer
        if (throttle_buffer_length > 0) {
            delayed_throttle = throttle_buffer[throttle_buffer_pointer];
            throttle_buffer[throttle_buffer_pointer] = throttle;
            throttle_buffer_pointer = (throttle_buffer_pointer + 1) % throttle_buffer_length;
        }
        else delayed_throttle = throttle;

        // Lag, with time constant ENGINE_LAG
        if (lag <= 0.0) k = 0.0;
        else k = pow(exp(-1.0), delta_t / lag);
        lagged_throttle = k * lagged_throttle + (1.0 - k) * delayed_throttle;

        last_time_lag_updated = simulation_time;
    }

    if (stabilized_attitude && (stabilized_attitude_angle_lat == 0) && (stabilized_attitude_angle_long == 0)) { // specific solution, avoids rounding errors in the more general calculation below
        b = lagged_throttle * MAX_THRUST * position.norm();
    }
    else {
        a.x = 0.0; a.y = 0.0; a.z = lagged_throttle * MAX_THRUST;
        xyz_euler_to_matrix(orientation, m);
        b.x = m[0] * a.x + m[4] * a.y + m[8] * a.z;
        b.y = m[1] * a.x + m[5] * a.y + m[9] * a.z;
        b.z = m[2] * a.x + m[6] * a.y + m[10] * a.z;
    }
    return b;
}

void update_lander_state(void)
// Called at every simulation step, once 'altitude' has been updated
{

    if (landed != true) {

        // This needs to be called every time step, even if the close-up view is not being rendered,
        // since any-angle attitude stabilizers reference closeup_coords.right
        update_closeup_coords();

        // Update historical record
        last_position = position;
        
        // Mechanical dynamics
        numerical_dynamics();
        
        // Refresh the visualization
        update_visualization();
    }
        
}

void reset_simulation(void)
// Resets the simulation to the initial state
{
    vector3d p, tv;
    unsigned long i;

    frames_until_update = 3;

    // Reset these three lander parameters here, so they can be overwritten in initialize_simulation() if so desired
    stabilized_attitude_angle_lat = 0;
    stabilized_attitude_angle_long = 0;
    attitude_stabilizer_mode = CLOSE_UP;
    throttle = 0.0;
    fuel = 1.0;
    autopilot_active = false;
    autopilot_mode = LAND;

    // Restore initial lander state
    initialize_simulation();

    // Check whether the lander is underground - if so, make sure it doesn't move anywhere
    landed = false;
    crashed = false;
    exploded = false;
    altitude = position.abs() - MARS_RADIUS;
    if (altitude < LANDER_SIZE / 2.0) {
        landed = true;
        velocity = vector3d(0.0, 0.0, 0.0);
    }

    // Visualisation routine's record of various speeds and velocities
    velocity_from_positions = velocity;
    last_position = position - delta_t * velocity_from_positions;
    previous_position = last_position;
    p = position.norm();
    climb_speed = velocity_from_positions * p;
    tv = velocity_from_positions - climb_speed * p;
    static_ground_speed = tv.abs();
    surface_ground_speed = (tv - atmosphere_velocity(position)).abs();
    surface_ground_speed_lat = ((tv - atmosphere_velocity(position)) * ((vector3d(0, 0, 1) ^ position) ^ position).norm());
    surface_ground_speed_long = ((tv - atmosphere_velocity(position)) * (vector3d(0, 0, 1) ^ position).norm());
    alt_list.clear();
    target_v_list.clear();
    v_list.clear();

    // Miscellaneous state variables
    throttle_control = (short)(throttle * THROTTLE_GRANULARITY + 0.5);
    simulation_time = 0.0;
    track.n = 0;
    parachute_lost = false;
    closeup_coords.initialized = false;
    closeup_coords.backwards = false;
    closeup_coords.right = vector3d(1.0, 0.0, 0.0);
    update_closeup_coords();
    camera_rot_x = 0;
    camera_rot_y = 0;
    camera_direction = position.toFVector();
    mars_rotation = 0.0f;
    surface_approach_location = vector3d(0, 0, 0);
    surface_approach_rotation = 0;
    previous_camera_rotation = vector3d(999, 999, 999);

    // Initialize the throttle history buffer
    if (delta_t > 0.0) throttle_buffer_length = (unsigned long)(ENGINE_DELAY / delta_t + 0.5);
    else throttle_buffer_length = 0;
    if (throttle_buffer_length > 0) {
        if (throttle_buffer != NULL) delete[] throttle_buffer;
        throttle_buffer = new double[throttle_buffer_length];
        for (i = 0; i < throttle_buffer_length; i++) throttle_buffer[i] = throttle;
        throttle_buffer_pointer = 0;
    }

}

//Reminder of original program's control scheme
/*
void glut_special (int key, int x, int y)
  // Callback for special key presses in all windows
{
  switch(key) {
  case GLUT_KEY_UP: // throttle up
    if (!autopilot_enabled && !landed && (fuel>0.0)) {
      throttle_control++;
      if (throttle_control>THROTTLE_GRANULARITY) throttle_control = THROTTLE_GRANULARITY;
      throttle = (double)throttle_control/THROTTLE_GRANULARITY;
    }
    break;
  case GLUT_KEY_DOWN: // throttle down
    if (!autopilot_enabled && !landed) {
      throttle_control--;
      if (throttle_control<0) throttle_control = 0;
      throttle = (double)throttle_control/THROTTLE_GRANULARITY;
    }
    break;
  case GLUT_KEY_RIGHT: // faster simulation
    simulation_speed++;
    if (simulation_speed>10) simulation_speed = 10;
    if (paused) {
      if (!landed) glutIdleFunc(update_lander_state);
      paused = false;
    }
    break;
  case GLUT_KEY_LEFT: // slower simulation
    simulation_speed--;
    if (simulation_speed<0) simulation_speed = 0;
    if (!simulation_speed) {
      glutIdleFunc(NULL);
      paused = true;
    }
    break;
  }
  if (paused || landed) refresh_all_subwindows();
}

void glut_key (unsigned char k, int x, int y)
  // Callback for key presses in all windows
{
  switch(k) {

  case 27: case 'q': case 'Q':
    // Escape or q or Q  - exit
    exit(0);
    break;

  case '0':
    // switch to scenario 0
    scenario = 0;
    reset_simulation();
    break;

  case '1':
    // switch to scenario 1
    scenario = 1;
    reset_simulation();
    break;

  case '2':
    // switch to scenario 2
    scenario = 2;
    reset_simulation();
    break;

  case '3':
    // switch to scenario 3
    scenario = 3;
    reset_simulation();
    break;

  case '4':
    // switch to scenario 4
    scenario = 4;
    reset_simulation();
    break;

  case '5':
    // switch to scenario 5
    scenario = 5;
    reset_simulation();
    break;

  case '6':
    // switch to scenario 6
    scenario = 6;
    reset_simulation();
    break;

  case '7':
    // switch to scenario 7
    scenario = 7;
    reset_simulation();
    break;

  case '8':
    // switch to scenario 8
    scenario = 8;
    reset_simulation();
    break;

  case '9':
    // switch to scenario 9
    scenario = 9;
    reset_simulation();
    break;

  case 'a': case 'A':
    // a or A - autopilot
    autopilot_enabled = !autopilot_enabled;
    autopilot_active = FALSE;
    if (paused) refresh_all_subwindows();
    break;

  case 'h': case 'H':
    // h or H - help
    if (help) {
      help = false;
      if (save_orbital_zoom > 0.0) orbital_zoom = save_orbital_zoom;
    } else {
      help = true;
      save_orbital_zoom = orbital_zoom;
      orbital_zoom = 0.4;
    }
    set_orbital_projection_matrix();
    if (paused || landed) refresh_all_subwindows();
    break;

  case 'l': case 'L':
    // l or L - toggle lighting model
    static_lighting = !static_lighting;
    glutSetWindow(orbital_window); enable_lights();
    glutSetWindow(closeup_window); enable_lights();
    if (paused || landed) refresh_all_subwindows();
    break;

  case 't': case 'T':
    // t or T - terrain texture
    do_texture = !do_texture;
    if (!texture_available) do_texture = false;
    if (paused || landed) refresh_all_subwindows();
    break;

  case 'p': case 'P':
    // p or P - deploy parachute
    if (!autopilot_enabled && !landed && (parachute_status == NOT_DEPLOYED)) parachute_status = DEPLOYED;
    if (paused) refresh_all_subwindows();
    break;

  case 's': case 'S':
    // s or S - attitude stabilizer
    if (!autopilot_enabled && !landed) stabilized_attitude = !stabilized_attitude;
    if (paused) refresh_all_subwindows();
    break;

  case 32:
    // space bar
    simulation_speed = 0;
    glutIdleFunc(NULL);
    if (paused && !landed) update_lander_state();
    else refresh_all_subwindows();
    paused = true;
    break;

  case 'z': case 'Z':
      // z or Z - increase attitude angle
      if (!autopilot_enabled && !landed) {
          stabilized_attitude_angle_lat += M_PI / 20;
          if (stabilized_attitude_angle_lat > M_PI - SMALL_NUM) stabilized_attitude_angle_lat = -M_PI;
      }
      if (paused) refresh_all_subwindows();
      break;

  case 'x': case 'X':
      // x or X - decrease attitude angle
      if (!autopilot_enabled && !landed) {
          stabilized_attitude_angle_lat -= M_PI / 20;
          if (stabilized_attitude_angle_lat < -M_PI + SMALL_NUM) stabilized_attitude_angle_lat = M_PI;
      }
      if (paused) refresh_all_subwindows();
      break;

  case 'c': case 'C':
      // c or C - reset attitude angle
      if (!autopilot_enabled && !landed) stabilized_attitude_angle_lat = 0;
      if (paused) refresh_all_subwindows();
      break;

  case 'v': case 'V':
      // v or V - change autopilot mode
      if (!autopilot_active) {
          if (autopilot_mode != TAKE_OFF) {
              autopilot_mode = static_cast<autopilot_mode_t>(static_cast<int>(autopilot_mode) + 1);
          }
          else autopilot_mode = LAND;
      }
      if (paused) refresh_all_subwindows();
      //cout << autopilot_mode;
      break;

  case 'b':
      // \ or * - start autopilot manouver
      if (autopilot_enabled && !autopilot_active) autopilot_active = TRUE;
      if (paused) refresh_all_subwindows();
      break;
  }
}
*/

vector3d axis_angle_rotation(vector3d v, vector3d k, double angle)
//Rotates vector 'v' by angle 'angle' about axis 'k'
{
    return v * cos(angle) + (k ^ v) * sin(angle) + k * (k * v) * (1 - cos(angle));
}

double angle_between_vectors(vector3d x, vector3d y)
//Returns the angle in rad between vectors x and y
{
    return acos((x * y) / (x.abs() * y.abs()));
}

double lowest_radius_of_orbit(vector3d a_position, vector3d a_velocity)
//Calculates the lowest radius of the orbit defined by position and velocity
{
    double gamma, c, r, v, b, r1, r2;
    r = a_position.abs();
    v = a_velocity.abs();
    gamma = angle_between_vectors(a_position, a_velocity);
    c = (2 * GRAVITY * MARS_MASS) / (r * pow(v, 2));
    b = sqrt(pow(c, 2) - 4 * (1 - c) * (-pow(sin(gamma), 2)));
    r1 = abs((-c + b) / (2 * (1 - c)) * r);
    r2 = abs((-c - b) / (2 * (1 - c)) * r);
    if (r1 < r2) return r1;
    else return r2;
}

vector3d atmosphere_velocity(vector3d b_position)
//Calculates the absolute velocity of the martian atmosphere at point 'position'
{
    double omega, magnitude;
    vector3d direction;
    omega = (2 * PI) / MARS_DAY;
    magnitude = omega * b_position.abs() * sin(angle_between_vectors(vector3d(0.0, 0.0, 1.0), b_position));
    direction = (vector3d(0.0, 0.0, 1.0) ^ b_position).norm();
    return magnitude * direction;
}

void velocity_account_for_surface(void)
//Adjust velocity so that lander is moving with the surface
{
    velocity += atmosphere_velocity(position);
}

vector3d wind_velocity(vector3d c_position)
//Returns the velocity of the martian wind at point 'position'
{
    double magnitude, variable_lat, variable_long;
    vector3d direction, constant_wind, variable_wind, relative_position;
    
    //Constant westerly wind at 'magnitude' m/s
    magnitude = 5;
    direction = (vector3d(0.0, 0.0, 1.0) ^ c_position).norm();
    constant_wind = magnitude * direction;

    //Add 'random' wind generated from coherent noise, travelling along the surface
    relative_position = position_relative_to_surface(c_position);
    relative_position = axis_angle_rotation(relative_position, vector3d(0, 0, 1), 0.00001 * simulation_time);
    variable_lat = wind_gusts_n->GetNoise3D(relative_position.x, relative_position.y, relative_position.z);
    variable_long = wind_gusts_e->GetNoise3D(relative_position.x, relative_position.y, relative_position.z);
    variable_wind = (variable_lat * ((vector3d(0, 0, 1) ^ c_position) ^ c_position).norm() + variable_long * (vector3d(0, 0, 1) ^ c_position).norm()) * 5;

    return constant_wind + variable_wind;
}

vector3d position_relative_to_surface(vector3d d_position)
//Returns the lander's posiiton relative to a fixed mars
{
    return axis_angle_rotation(d_position, vector3d(0, 0, 1), -2 * PI * simulation_time / MARS_DAY);
}

void initialise_program(void)
//Start simulation and reset global variables
{
    scenario = 0;
    simulation_speed = 5;
    camera_zoom = 600;
    help = false;
    paused = false;
    reset_simulation();
    microsecond_time(time_program_started);
}

void autopilot(void)
// Autopilot to adjust the engine throttle, parachute and attitude control
{

    double k_h, k_h_h, k_p, delta, p_out, p_out_h, p_out_h_lat, p_out_h_long, target_descent_rate, target_ascent_rate, target_horizontal_rate, error, error_h_lat, error_h_long, error_h;

    if (autopilot_active)
        //Only complete a manouver once activated
    {

        stabilized_attitude = true;

        if (autopilot_mode == LAND) {
            //Slow down until the lander's orbital trajectory passes within 5km of the surface to ensure that the lander will come into land, then land safely
            if (lowest_radius_of_orbit(position, velocity) > MARS_RADIUS + 5000)
                //Drop out of orbit
            {
                attitude_stabilizer_mode = CLOSE_UP;

                //Point backwards to the direction of motion
                stabilized_attitude_angle_lat = -angle_between_vectors(position, velocity);
                stabilized_attitude_angle_long = 0;
                throttle = 1;

            }
            else {

                attitude_stabilizer_mode = LAT_LONG;

                stabilized_attitude_angle_lat = 0;
                stabilized_attitude_angle_long = 0;

                k_h = 0.03;
                k_h_h = 0.05;
                k_p = 1;
                delta = 0.55;

                //Compare vertical, north/south and east/west speeds to the defined linear decrease
                target_descent_rate = -(0.5 + k_h * (position.abs() - current_radius));
                error = -(0.5 + k_h * (position.abs() - current_radius) + (climb_speed));
                error_h_lat = (0 + k_h_h * (position.abs() - current_radius) - abs(surface_ground_speed_lat));
                error_h_long = (0 + k_h_h * (position.abs() - current_radius) - abs(surface_ground_speed_long));

                p_out = k_p * error + delta;
                p_out_h_lat = k_p * error_h_lat;
                p_out_h_long = k_p * error_h_long;

                //Only try to correct against n/s, e/w speed when near the surface and moving quickly to allow the parachute and lander drag to reduce these speeds before final corrections
                if (p_out <= 0) p_out = 0;
                if (abs(surface_ground_speed_lat) < abs(k_h_h * altitude) || altitude > 5000) p_out_h_lat = 0;
                if (abs(surface_ground_speed_long) < abs(k_h_h * altitude) || altitude > 5000) p_out_h_long = 0;

                //Set engine power
                throttle = sqrt(pow(p_out, 2) + pow(p_out_h_lat, 2) + pow(p_out_h_long, 2));
                if (throttle == 0) {
                    //Point upwards when thruster is not needed
                    stabilized_attitude_angle_lat = 0;
                    stabilized_attitude_angle_long = 0;
                }
                else {
                    //Set lander orientation to correct for motion in 3 axes
                    if (surface_ground_speed_lat > 0) stabilized_attitude_angle_lat = -(-atan2(p_out, p_out_h_lat) + PI / 2);
                    else stabilized_attitude_angle_lat = -atan2(p_out, p_out_h_lat) + PI / 2;
                    if (surface_ground_speed_long > 0) stabilized_attitude_angle_long = -(-atan2(p_out, p_out_h_long) + PI / 2);
                    else stabilized_attitude_angle_long = -atan2(p_out, p_out_h_long) + PI / 2;
                }
                
                //Record altitude and current/target velocities
                alt_list.push_back(altitude);
                target_v_list.push_back(target_descent_rate);
                v_list.push_back((velocity * position.norm()));

                if (parachute_status != DEPLOYED && safe_to_deploy_parachute() && (velocity * position.norm()) < 0 && target_descent_rate > -MAX_PARACHUTE_SPEED && -0.5 * atmospheric_density(position) * DRAG_COEF_CHUTE * (5 * pow(2 * LANDER_SIZE, 2)) * pow(target_descent_rate, 2) < MAX_PARACHUTE_DRAG)
                    //Deploy parachute once safe to do so, ensuring that the parachute will not rip off or burn up after deployment
                {
                    parachute_status = DEPLOYED;
                }

            }
        }



        if (autopilot_mode == TAKE_OFF)
            //Launch Lander into orbit at altitude 'target_altitude' above MARS_RADIUS
        {
            attitude_stabilizer_mode = CLOSE_UP;

            //Calculate the speed required for a circular orbit at altitude 'target_altitude'
            double target_static_ground_speed;
            target_static_ground_speed = sqrt((GRAVITY * MARS_MASS) / (MARS_RADIUS + target_altitude));


            if (abs(static_ground_speed - target_static_ground_speed) > 0.5 || abs(position.abs() - MARS_RADIUS - target_altitude) > 1)
                //Run the autopilot until in a stable orbit at the specified altitude
            {

                //Define a linear decrease in vertical speed up to the orbital altitude, and a linear increase in horizontal speed
                k_h = 0.001;
                k_h_h = sqrt((GRAVITY * MARS_MASS) / (MARS_RADIUS + target_altitude)) / target_altitude;
                k_p = 1;

                target_ascent_rate = (0 - k_h * (position.abs() - MARS_RADIUS - target_altitude));
                target_horizontal_rate = (target_static_ground_speed - k_h_h * (position.abs() - MARS_RADIUS - target_altitude));
                error = (0 - k_h * (position.abs() - MARS_RADIUS - target_altitude) - (climb_speed));
                if (position.abs() < (MARS_RADIUS + target_altitude)) {
                    //Linear change between comparing horizontal speed against the surface of mars, and in an absolute reference frame to prevent correcting speed against the atmosphere
                    if (position.abs() > (MARS_RADIUS + EXOSPHERE)) {
                        error_h = (target_static_ground_speed + k_h_h * (position.abs() - MARS_RADIUS - target_altitude) - (static_ground_speed));
                    }
                    else {
                        error_h = (1 - (position.abs() - current_radius) / EXOSPHERE) * (target_static_ground_speed + k_h_h * (position.abs() - MARS_RADIUS - target_altitude) - (surface_ground_speed)) + ((position.abs() - current_radius) / EXOSPHERE) * (target_static_ground_speed + k_h_h * (position.abs() - MARS_RADIUS - target_altitude) - (static_ground_speed));
                    }
                }
                else {
                    //When above the target altitude, attempt to stay in a circular orbit to prevent entering highly eliptical orbits when descending
                    error_h = (sqrt((GRAVITY * MARS_MASS) / position.abs()) - static_ground_speed);
                }
                p_out = k_p * error;
                //Correct against the weight of the lander if it's trajectory shows it descending back to the surface
                if (lowest_radius_of_orbit(position, velocity) < MARS_RADIUS + 5000) p_out += ((((GRAVITY * MARS_MASS * (UNLOADED_LANDER_MASS + fuel * FUEL_CAPACITY * FUEL_DENSITY)) / position.abs2()) * position.norm()).abs()) / MAX_THRUST;
                p_out_h = k_p * error_h;

                //Stabilize the lander to provide the appropriate amount of vertical and horizontal thrust
                stabilized_attitude_angle_lat = -atan2(p_out, p_out_h) + PI / 2;
                throttle = sqrt(pow(p_out, 2) + pow(p_out_h, 2));

            }
            else {
                //Deactivate the autopilot once in the specified orbit
                stabilized_attitude_angle_lat = 0;
                stabilized_attitude_angle_long = 0;
                throttle = 0;
                autopilot_active = false;
            }
        }
    }
}

void numerical_dynamics(void)
// This is the function that performs the numerical integration to update the
// lander's pose. The time step is delta_t (global variable).
{

    //calculate new position and velocity

    vector3d acceleration, gravitational_acceleration, thrust_force, lander_drag_force, parachute_drag_force, temp_position, velocity_through_atmosphere;

    //Calculate all forces and accelerations on the lander
    gravitational_acceleration = -((GRAVITY * MARS_MASS) / position.abs2()) * position.norm();
    thrust_force = thrust_wrt_world();
    velocity_through_atmosphere = velocity - atmosphere_velocity(position) - wind_velocity(position);
    lander_drag_force = -0.5 * atmospheric_density(position) * DRAG_COEF_LANDER * (PI * pow(LANDER_SIZE, 2)) * velocity_through_atmosphere.abs2() * velocity_through_atmosphere.norm();
    parachute_drag_force = -0.5 * atmospheric_density(position) * DRAG_COEF_CHUTE * (5 * pow(2 * LANDER_SIZE, 2)) * velocity_through_atmosphere.abs2() * velocity_through_atmosphere.norm();
    drag_force = lander_drag_force;
    if (parachute_status == DEPLOYED) {
        drag_force += parachute_drag_force;
    }
    acceleration = gravitational_acceleration + (thrust_force + drag_force) / (UNLOADED_LANDER_MASS + fuel * FUEL_CAPACITY * FUEL_DENSITY);

    //Verlet Integrator

    temp_position = position;
    position = 2 * position - previous_position + pow(delta_t, 2) * acceleration;
    previous_position = temp_position;
    velocity = (position - previous_position) / delta_t;


    //Euler Integrator (Inaccurate)
    /*
    position = position + delta_t * velocity;
    velocity = velocity + delta_t * acceleration;
    */


    // Here we can apply an autopilot to adjust the thrust, parachute and attitude
    if (autopilot_enabled) autopilot();

    // Here we can apply 3-axis stabilization to ensure the base is always pointing downwards
    if (stabilized_attitude) attitude_stabilization();
}

void initialize_simulation(void)
// Lander pose initialization - selects one of 10 possible scenarios
{
    // The parameters to set are:
    // position - in Cartesian planetary coordinate system (m)
    // velocity - in Cartesian planetary coordinate system (m/s)
    // orientation - in lander coordinate system (xyz Euler angles, degrees)
    // delta_t - the simulation time step
    // boolean state variables - parachute_status, stabilized_attitude, autopilot_enabled
    // scenario_description - a descriptive string for the help screen


    scenario_description[0] = "circular orbit";
    scenario_description[1] = "descent from 10km";
    scenario_description[2] = "elliptical orbit, thrust changes orbital plane";
    scenario_description[3] = "polar launch at escape velocity (but drag prevents escape)";
    scenario_description[4] = "elliptical orbit that clips the atmosphere and decays";
    scenario_description[5] = "descent from 200km";
    scenario_description[6] = "areostationary orbit";
    scenario_description[7] = "";
    scenario_description[8] = "";
    scenario_description[9] = "";
    

    
    switch (scenario) {

    case 0:
        // a circular equatorial orbit
        position = vector3d(1.2 * MARS_RADIUS, 0.0, 0.0);
        velocity = vector3d(0.0, -3247.087385863725, 0.0);
        orientation = vector3d(0.0, 90.0, 0.0);
        delta_t = 0.1;
        parachute_status = NOT_DEPLOYED;
        stabilized_attitude = true;
        autopilot_enabled = false;
        break;

    case 1:
        // a descent from rest at 10km altitude
        position = vector3d(0.0, -(MARS_RADIUS + 10000.0 + 7500.0), 0.0);
        velocity = vector3d(0.0, 0.0, 0.0);
        velocity_account_for_surface();
        orientation = vector3d(0.0, 0.0, 90.0);
        delta_t = 0.1;
        parachute_status = NOT_DEPLOYED;
        stabilized_attitude = true;
        autopilot_enabled = false;
        break;

    case 2:
        // an elliptical polar orbit
        position = vector3d(0.0, 0.0, 1.2 * MARS_RADIUS);
        velocity = vector3d(3500.0, 0.0, 0.0);
        orientation = vector3d(0.0, 0.0, 90.0);
        delta_t = 0.1;
        parachute_status = NOT_DEPLOYED;
        stabilized_attitude = true;
        autopilot_enabled = false;
        break;

    case 3:
        // polar surface launch at escape velocity (but drag prevents escape)
        position = vector3d(0.0, 0.0, 3394200.0);
        //position = vector3d(0.0, 0.0, MARS_RADIUS + LANDER_SIZE / 2.0);
        velocity = vector3d(0.0, 0.0, 5027.0);
        velocity_account_for_surface();
        orientation = vector3d(0.0, 0.0, 0.0);
        delta_t = 0.1;
        parachute_status = NOT_DEPLOYED;
        stabilized_attitude = false;
        autopilot_enabled = false;
        break;

    case 4:
        // an elliptical orbit that clips the atmosphere each time round, losing energy
        position = vector3d(0.0, 0.0, MARS_RADIUS + 100000.0);
        velocity = vector3d(4000.0, 0.0, 0.0);
        orientation = vector3d(0.0, 90.0, 0.0);
        delta_t = 0.1;
        parachute_status = NOT_DEPLOYED;
        stabilized_attitude = false;
        autopilot_enabled = false;
        break;

    case 5:
        // a descent from rest at the edge of the exosphere
        position = vector3d(0.0, -(MARS_RADIUS + EXOSPHERE + 7500), 0.0);
        velocity = vector3d(0.0, 0.0, 0.0);
        velocity_account_for_surface();
        orientation = vector3d(0.0, 0.0, 90.0);
        delta_t = 0.1;
        parachute_status = NOT_DEPLOYED;
        stabilized_attitude = true;
        autopilot_enabled = false;
        break;

    case 6:
        // areostationary orbit around the equator
        position = vector3d(cbrt((GRAVITY * MARS_MASS * pow(MARS_DAY, 2)) / (4 * pow(PI, 2))), 0.0, 0.0);
        velocity = vector3d(0.0, (2 * PI * (cbrt((GRAVITY * MARS_MASS * pow(MARS_DAY, 2)) / (4 * pow(PI, 2))))) / (MARS_DAY), 0.0);
        orientation = vector3d(0.0, 0.0, 90.0);
        delta_t = 0.1;
        parachute_status = NOT_DEPLOYED;
        stabilized_attitude = false;
        autopilot_enabled = false;
        break;

    }
    
}