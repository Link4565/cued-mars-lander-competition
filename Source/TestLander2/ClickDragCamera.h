// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TestActor.h"
#include "ClickDragCamera.generated.h"

/**
 * 
 */
UCLASS()
class TESTLANDER2_API AClickDragCamera : public APlayerController
{
	GENERATED_BODY()
	
	AClickDragCamera();

	//Declare camera movement functions

	UFUNCTION(BlueprintCallable, Category = MouseRotate)
	void ModifyCameraRotation(float mouseX, float mouseY, float strength);

	UFUNCTION(BlueprintCallable, Category = MouseZoom)
	void ModifyCameraZoom(float mouseScrollAxis, float strength2);
};


