// Fill out your copyright notice in the Description page of Project Settings.


#include "LanderHUD.h"

float ULanderHUD::GetAltitude() {
	return altitude;
}

int ULanderHUD::GetAltitudeOrder() {
	if (altitude > 10) return floor(log10(altitude));
	else return 0;
}

float ULanderHUD::GetAltitudeAngle() {
	return FMath::GetMappedRangeValueClamped(FVector2D(0, 10), FVector2D(-135, 135), altitude / pow(10, GetAltitudeOrder()));
}

float ULanderHUD::GetClimbRate() {
	return climb_speed;
}

int ULanderHUD::GetClimbRateOrder() {
	if (abs(climb_speed) > 10) return floor(log10(abs(climb_speed)));
	else return 0;
}

float ULanderHUD::GetClimbRateAngle() {
	return FMath::GetMappedRangeValueClamped(FVector2D(0, 10), FVector2D(-135, 135), abs(climb_speed) / pow(10, GetClimbRateOrder()));
}


float ULanderHUD::GetGroundSpeed() {
	return surface_ground_speed;
}

int ULanderHUD::GetGroundSpeedOrder() {
	if (surface_ground_speed > 10) return floor(log10(surface_ground_speed));
	else return 0;
}

float ULanderHUD::GetGroundSpeedAngle() {
	return FMath::GetMappedRangeValueClamped(FVector2D(0, 10), FVector2D(-135, 135), surface_ground_speed / pow(10, GetGroundSpeedOrder()));
}


float ULanderHUD::GetSimulationSpeed() {
	return simulation_speed;
}


float ULanderHUD::GetTime() {
	return simulation_time;
}


FVector ULanderHUD::GetPosition() {
	return position.toFVector();
}


FVector ULanderHUD::GetVelocity() {
	return velocity.toFVector();
}


float ULanderHUD::GetThrottle() {
	return throttle;
}


float ULanderHUD::GetThrust() {
	return FVector(thrust_wrt_world().toFVector()).Size();
}


float ULanderHUD::GetFuel() {
	return fuel;
}

float ULanderHUD::GetFuelLitres() {
	return fuel * FUEL_CAPACITY;
}

FString ULanderHUD::GetScenario() {
	return FString::Printf(TEXT("Scenario %i: %s"), static_cast<int>(scenario), UTF8_TO_TCHAR(scenario_description[scenario].c_str()));
}


bool ULanderHUD::GetAutopilotEnabled() {
	return autopilot_enabled;
}


bool ULanderHUD::GetAutopilotActive() {
	return autopilot_active;
}


int ULanderHUD::GetAutopilotMode() {
	return autopilot_mode;
}


bool ULanderHUD::GetAttitudeStabilizerEnabled() {
	return stabilized_attitude;
}

int ULanderHUD::GetAttitudeStabilizerMode() {
	return attitude_stabilizer_mode;
}

float ULanderHUD::GetAttitudeStabilizerAngle() {
	return stabilized_attitude_angle_lat;
}


int ULanderHUD::GetParachuteStatus() {
	if (safe_to_deploy_parachute()) return parachute_status;
	else return 3;
}

bool ULanderHUD::GetPaused() {
	return paused;
}

bool ULanderHUD::GetHelp() {
	return help;
}

bool ULanderHUD::GetLanded() {
	return landed;
}

bool ULanderHUD::GetCrashed() {
	return crashed;
}